'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module)
        app.start();
});

// AQC_TTL -> TimeTokenLive genera un tiempo de vida de 14400 
app.use(loopback.token()); // You should have this already
app.use(function(req, res, next) {
    // Make sure this middleware is registered after loopback.token
    var token = req.accessToken;
    if (!token) {
        return next();
    }
    var now = new Date();
    if (now.getTime() - token.created.getTime() < 1000) {
        return next();
    }
    req.accessToken.created = now;
    req.accessToken.ttl = 10800; // 10800 three hours, 14400 four hours
    req.accessToken.save(next);
});
// <- AQC_TTL