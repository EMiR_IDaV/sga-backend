module.exports = function(app) {
    app.dataSources.files_local.connector.getFilename = function(file, req, res) {
        //file.name is original filename uploaded
        var filename = req.query.filename || 'general.jpg';
        return filename;
    }
};