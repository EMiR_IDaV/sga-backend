module.exports = function(app) {
    var ds = app.dataSources.mysqlDB;
    var dbTables = []; // En este array pondremos los nombres de los modelos
    if (dbTables.length !== 0) {
        ds.automigrate(dbTables, function(err) {
            if (err) throw err;

            console.log('Tabla/s => [' + dbTables + '] creadas.\nEn:', ds.adapter.name);
        });
    } else {
        console.log('No hay tablas por crear.');
    }
    //ds.disconnect(); //Descomentar si se pone fuera de la carpeta boot

    // dbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'usuario', 'estudiante','admin','caracteristica','curso','docente','trabajo','estudio','modulo'];
    // dbTables = ['NOTICIA'];11-04-2019 =====> Deben ir los nombres que figura en el .json en este caso el noticia.json >>>"name": "NOTICIA"<<<<

    var dbTablesUp = ['usuario', 'admin', 'estudiante', 'docente', 'curso', 'modulo', 'sitio_interes', 'consulta', 'nota', 'noticia', 'asistencia', 'planilla', 'documento', 'evento', 'proyecto', 'investiga', 'convocatoriainves', 'publica', 'unidad', 'red_social', 'contacto', 'oficina', 'recurso', 'participante', 'uInvestigacion', 'uInteraccion', 'uPosgrado', 'investigador', 'auxiliar']; // En este array pondremos los nombres de los modelos
    if (dbTablesUp.length !== 0) {
        ds.autoupdate(dbTablesUp, function(err) {
            if (err) throw err;

            console.log('Tabla/s => [' + dbTablesUp + '] actualizadas.\nEn:', ds.adapter.name);
        });
    } else {
        console.log('No hay tablas por actualizar.');
    }

    // ds.autoupdate()
    //     .then(function(err) {
    //         // Your code here
    //     });

};