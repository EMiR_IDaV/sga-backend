'use strict';
var app = require('../../server/server');

module.exports = function(Investigador) {


    /*------------- AQ Asignar ROLE docente al docente creado o actualizado  -------------*/
    Investigador.observe('after save', function(ctx, next) {
        var Role = app.models.Role;
        var RoleMapping = app.models.RoleMapping;

        Role.findOne({ where: { name: 'investigador' } }, function(err, result) {
            if (err) {
                return console.log(err);
            } else {

                RoleMapping.findOrCreate({ where: { principalId: ctx.instance.usuarioId, roleId: result.id } }, {
                    principalType: RoleMapping.USER,
                    principalId: ctx.instance.usuarioId,
                    roleId: result.id
                }, function(err, principal) {
                    // TODO handle error
                    if (err) throw err;
                    console.log('Created principal:', principal);
                    // if no errors, user has now the role administrator
                });
            }
        });
        next();
    });

};