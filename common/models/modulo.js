'use strict';
var app = require('../../server/server');

module.exports = function(Modulo) {

    /*------------- ELIMINA TODAS LOS REGISTROS DE NOTA* ASOCIADOS AL MODULO ELIMINADO -------------*/
    Modulo.observe('after delete', function(ctx, next) {

        console.log('Deleted %s matching %j',
            ctx.Model.pluralModelName,
            ctx.where.id);

        var Nota = app.models.Nota;

        Nota.destroyAll({ moduloId: ctx.where.id }, function(err, info) {
            if (err) {
                return console.log('err', err);
            } else {
                console.log('info', info);
            }
        });

        next();

    });

};