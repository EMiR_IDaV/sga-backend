'use strict';
var app = require('../../server/server');

module.exports = function(Docente) {


    /*------------- AQ Asignar ROLE docente al docente creado o actualizado  -------------*/
    Docente.observe('after save', function(ctx, next) {
        var Role = app.models.Role;
        var RoleMapping = app.models.RoleMapping;

        var Convocatoriainves = app.models.Convocatoriainves;

        Convocatoriainves.findOne({ where: {} })
        Role.findOne({ where: { name: 'docente' } }, function(err, result) {
            if (err) {
                return console.log(err);
            } else {

                RoleMapping.findOrCreate({ where: { principalId: ctx.instance.usuarioId, roleId: result.id } }, {
                    principalType: RoleMapping.USER,
                    principalId: ctx.instance.usuarioId,
                    roleId: result.id
                }, function(err, principal) {
                    // TODO handle error
                    if (err) throw err;
                    console.log('Created principal:', principal);
                    // if no errors, user has now the role administrator
                });
            }
        });
        next();
    });

};