'use strict';
var app = require('../../server/server');

module.exports = function(Curso) {

    // revisar antes de liberar no se necesita autenticacion, PERO DEBERIA DE
    /* 4. ------------- Método que retorna los documentos de un determinado curso que esta tomando un Estudiante de posgrado -------------*/
    Curso.cursoDocumentos = function(sigla, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT documento.*
                    FROM documento, modulo, curso
                    WHERE documento.moduloId = modulo.id && modulo.cursoId = curso.id && curso.sigla = ${sigla}`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Curso.remoteMethod('cursoDocumentos', {
        accepts: [
            { arg: 'sigla', type: 'string', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:sigla/documentos', verb: 'get', status: 200 }
    });

    /* 3. ------------- Dado una sigla y un numero de Modulo se obtiene el id del Usuario q dicta clases -------------*/
    Curso.idUsuario = function(sigla, numero, cb) {

        // console.log('typoSIGLA', typeof(sigla));

        // if (typeof(sigla) !== 'string') {
        //     cb(null, []);
        // }
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT docente.usuarioId
                        FROM curso, modulo, docente
                        where ${sigla} = curso.sigla && curso.id = modulo.cursoId &&
                                modulo.numero = ${numero} && modulo.docenteId = docente.id`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Curso.remoteMethod('idUsuario', {
        accepts: [
            { arg: 'sigla', type: 'string', required: true },
            { arg: 'numero', type: 'number', required: true }
        ],
        returns: { type: 'number', root: true },
        http: { path: '/:sigla/:numero/idUsuario', verb: 'get', status: 200 }
    });


    /* 2. ------------- ELIMINA UNO X UNO A LOS MODULOS* ASOCIADOS AL CURSO ELIMINADO   -------------*/
    Curso.observe('after delete', function(ctx, next) {

        console.log('Deleted %s matching %j',
            ctx.Model.pluralModelName,
            ctx.where.id);
        var Modulo = app.models.Modulo;
        Modulo.find({ where: { cursoId: ctx.where.id } }, function(err, resultObjects) {

            var lago = resultObjects.map(resultRaw => {
                console.log(resultRaw.id);
                Modulo.destroyById(resultRaw.id, function(err) {
                    var response = "Successfully removed";
                    console.log(response);
                });
            });

        });

        next();
    });

    /* 1. ------------- Propiedad que retorna los datos publicos del total de cargas horarios, creditos de un curso -------------*/
    Curso.total = function(id, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT   SUM(modulo.carga_horaria_a) tcarga_horaria_a, SUM(modulo.carga_horaria_b) tcarga_horaria_b,
                            SUM(modulo.horas_academicas) thoras_academicas, SUM(modulo.creditos) tcreditos
                                FROM modulo, curso WHERE curso.id = modulo.cursoId && curso.id = ${id}`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Curso.remoteMethod('total', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/total', verb: 'get', status: 200 }
    });

};

// ANTERIOR CONSULTA :D
// SELECT SUM(modulo.carga_horaria_a) tcarga_horaria_a, SUM(modulo.carga_horaria_b) tcarga_horaria_b,
//                             SUM(modulo.horas_academicas) thoras_academicas, SUM(modulo.creditos) tcreditos
//                     FROM
//                         modulo, curso
//                     WHERE
//                         curso.id = ${id} && modulo.cursoId = curso.id