'use strict';
var app = require('../../server/server');

module.exports = function(Estudiante) {

    Estudiante.observe('after save', function(ctx, next) {
        var Role = app.models.Role;
        var RoleMapping = app.models.RoleMapping;
        Role.findOne({ where: { name: 'estudiante' } }, function(err, result) {
            if (err) {
                return console.log(err);
            } else {

                RoleMapping.findOrCreate({ where: { principalId: ctx.instance.usuarioId, roleId: result.id } }, {
                    principalType: RoleMapping.USER,
                    principalId: ctx.instance.usuarioId,
                    roleId: result.id
                }, function(err, principal) {
                    // TODO handle error
                    if (err) throw err;
                    console.log('Created principal:', principal);
                    // if no errors, user has now the role administrator
                });
            }
        });
        next();
    });

    /*------------- AQ Asignar role estudiante al estudiante creado o actualizado  MEJORADO-------------*/
    // Estudiante.observe('after save', function(ctx, next) {
    //     console.log(ctx.instance.type);
    //     var Role = app.models.Role;
    //     var RoleMapping = app.models.RoleMapping;
    //     Role.findOne({ where: { name: 'estudiante' } }, function(err, result) {
    //         if (err) {
    //             return console.log(err);
    //         } else {

    //             RoleMapping.findOrCreate({ where: { principalId: ctx.instance.usuarioId, roleId: result.id } }, {
    //                 principalType: RoleMapping.USER,
    //                 principalId: ctx.instance.usuarioId,
    //                 roleId: result.id
    //             }, function(err, principal) {
    //                 // TODO handle error
    //                 if (err) throw err;
    //                 console.log('Created principal:', principal);
    //                 // if no errors, user has now the role administrator
    //             });
    //         }
    //     });
    //     next();
    // });
}

//*------------- AQ Asignar role estudiante al estudiante creado o actualizado  FUNCIONA-------------*/
// Estudiante.observe('after save', function(ctx, next) {
//     var Role = app.models.Role;
//     var RoleMapping = app.models.RoleMapping;
//     Role.find({ where: { name: 'estudiante' } }, function(err, results) {
//         if (err) {
//             return console.log(err);
//         } else {

//             RoleMapping.find({ where: { principalId: ctx.instance.usuarioId, roleId: results[0].id } }, function(err, result) {
//                 if (err) {
//                     return console.log(err);
//                 } else {
//                     if (result.length == 0) {
//                         //asignar al estudiante creado o actualizado el role 'estudiante'
//                         RoleMapping.create({
//                             principalType: RoleMapping.USER,
//                             principalId: ctx.instance.usuarioId,
//                             roleId: results[0].id
//                         }, function(err, principal) {
//                             if (err) throw err;
//                             console.log('Created principal:', principal);
//                         });

//                     } else {
//                         console.log('ya existe un mapeo');
//                     }
//                 }
//             });
//         }
//     });
//     next();
// });




// Estudiante.observe('after save', function(ctx, next) {
//             if (ctx.instance) {
//                 console.log('Saved %s#%s', ctx.Model.modelName, ctx.instance.usuarioId);

//                 //create the admin role
//                 Role.create({
//                     name: 'admin'
//                 }, function(err, role) {
//                     if (err) throw err;

//                     console.log('Created role:', role);

//                     //make bob an admin
//                     role.principals.create({
//                         principalType: RoleMapping.USER,
//                         principalId: ctx.instance.usuarioId
//                     }, function(err, principal) {
//                         if (err) throw err;

//                         console.log('Created principal:', principal);
//                     });
//                 });
//             } else {
//                 console.log('Updated %s matching %j',
//                     ctx.Model.pluralModelName,
//                     ctx.where);
//             }
//             next();
//         });



// module.exports = function(Estudiante) {

//         var Population = app.models.Population;
//         Estudiante.GetCurrentPopulation = function(req) {
//             Population.find({ where { id: req.id } }, function(err) {
//                     if (err) {
//                         return console.log(err);
//                     } else {
//                         // do something here 
//                     });
//             }
//         }
//     }



// module.exports = function(Estudiante) {

//     var User = app.models.user;
//     var Role = app.models.Role;
//     var Curso = app.models.curso;
//     var RoleMapping = app.models.RoleMapping;
//     var Team = app.models.Team;
//     Estudiante.observe('after save', function(ctx, next) {
//         console.log(Curso);
//         if (ctx.instance) {
//             console.log('Saved %s#%s', ctx.Model.modelName, ctx.instance.usuarioId);

//             //create the admin role
//             Role.create({
//                 name: 'admin'
//             }, function(err, role) {
//                 if (err) throw err;

//                 console.log('Created role:', role);

//                 //make bob an admin
//                 role.principals.create({
//                     principalType: RoleMapping.USER,
//                     principalId: ctx.instance.usuarioId
//                 }, function(err, principal) {
//                     if (err) throw err;

//                     console.log('Created principal:', principal);
//                 });
//             });
//         } else {
//             console.log('Updated %s matching %j',
//                 ctx.Model.pluralModelName,
//                 ctx.where);
//         }
//         next();
//     });

// };

// var User = app.models.user;
//   var Role = app.models.Role;
//   var RoleMapping = app.models.RoleMapping;
//   var Team = app.models.Team;

//   User.create([
//     {username: 'John', email: 'john@doe.com', password: 'opensesame'},
//     {username: 'Jane', email: 'jane@doe.com', password: 'opensesame'},
//     {username: 'Bob', email: 'bob@projects.com', password: 'opensesame'}
//   ], function(err, users) {
//     if (err) throw err;

//     console.log('Created users:', users);

//     // create project 1 and make john the owner
//     users[0].projects.create({
//       name: 'project1',
//       balance: 100
//     }, function(err, project) {
//       if (err) throw err;

//       console.log('Created project:', project);

//       // add team members
//       Team.create([
//         {ownerId: project.ownerId, memberId: users[0].id},
//         {ownerId: project.ownerId, memberId: users[1].id}
//       ], function(err, team) {
//         if (err) throw err;

//         console.log('Created team:', team);
//       });
//     });

//     //create project 2 and make jane the owner
//     users[1].projects.create({
//       name: 'project2',
//       balance: 100
//     }, function(err, project) {
//       if (err) throw err;

//       console.log('Created project:', project);

//       //add team members
//       Team.create({
//         ownerId: project.ownerId,
//         memberId: users[1].id
//       }, function(err, team) {
//         if (err) throw err;

//         console.log('Created team:', team);
//       });
//     });

//     //create the admin role
//     Role.create({
//       name: 'admin'
//     }, function(err, role) {
//       if (err) throw err;

//       console.log('Created role:', role);

//       //make bob an admin
//       role.principals.create({
//         principalType: RoleMapping.USER,
//         principalId: users[2].id
//       }, function(err, principal) {
//         if (err) throw err;

//         console.log('Created principal:', principal);
//       });
//     });
//   });