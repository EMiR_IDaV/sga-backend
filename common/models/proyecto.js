'use strict';
var app = require('../../server/server');


module.exports = function(Proyecto) {

    /* 1. ------------- Retorna los proyectos de una Convocatoria dada -------------*/
    Proyecto.proyectosPorConvocatoria = function(idConvocatoria, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT DISTINCT proyecto.*
                    FROM proyecto, publica, investiga
                    WHERE proyecto.id = publica.proyectoId && publica.investigaId =investiga.id
                        && investiga.convocatoriainvesId = ${idConvocatoria}`;

        ds.connector.execute(sql, (err, resultObjects) => {
            if (!resultObjects.length) return cb(null, []);
            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Proyecto.remoteMethod('proyectosPorConvocatoria', {
        accepts: [
            { arg: 'idConvocatoria', type: 'number', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:idConvocatoria/proyectosPorConvocatoria', verb: 'get', status: 200 }
    });

};