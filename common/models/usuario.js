'use strict';
var app = require('../../server/server');

module.exports = function(Usuario) {

    /* 26.-   ------------- Retorna datos publicos de UN AUXILIAR por su codimagen -------------*/
    Usuario.publicAuxiliar = function(gestion, docinv, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    auxiliar.descripcion, auxiliar.resena, auxiliar.imgpath, auxiliar.denominacion
                    FROM convocatoriainves, usuario, auxiliar 
                    WHERE convocatoriainves.gestion = '${gestion}' && convocatoriainves.id = auxiliar.convocatoriainvesId
                    && auxiliar.denominacion = '${docinv}' && auxiliar.usuarioId = usuario.id`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const auxiliarRaw = ds.connector.fromRow('auxiliar', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const auxiliar = new Usuario.app.models.auxiliar(auxiliarRaw);

                // Set the auxiliar on the book object
                usuario.auxiliar(auxiliar);
                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('publicAuxiliar', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.auxiliar = result.__cachedRelations.auxiliar.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicAuxiliar', {
        accepts: [
            { arg: 'gestion', type: 'string', required: true },
            { arg: 'docinv', type: 'string', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:gestion/publicAuxiliar/:docinv', verb: 'get', status: 200 }
    });

    /* 25.- ------------- Retorna datos Públicos de Auxiliares que colaboraron en una Convocatoria incluido auxiliar -------------*/
    Usuario.publicAuxiliaresPorConvocatoria = function(gestionConv, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    auxiliar.denominacion, auxiliar.resena, auxiliar.imgpath, auxiliar.descripcion
                    FROM usuario, auxiliar, convocatoriainves
                    WHERE usuario.id = auxiliar.usuarioId && auxiliar.convocatoriainvesId = convocatoriainves.id && convocatoriainves.gestion = ${gestionConv}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const auxiliarRaw = ds.connector.fromRow('auxiliar', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const auxiliar = new Usuario.app.models.auxiliar(auxiliarRaw);

                // Set the auxiliar on the book object
                usuario.auxiliar(auxiliar);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('publicAuxiliaresPorConvocatoria', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.auxiliar = result.__cachedRelations.auxiliar.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicAuxiliaresPorConvocatoria', {
        accepts: [
            { arg: 'gestionConv', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:gestionConv/publicAuxiliaresPorConvocatoria', verb: 'get', status: 200 }
    });

    /* 24.- ------------- Retorna datos Públicos de Auxiliares que colaboraron en un Proyecto incluido auxiliar -------------*/
    Usuario.publicAuxiliaresPorProyecto = function(idProyecto, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    auxiliar.denominacion, auxiliar.resena, auxiliar.imgpath, auxiliar.descripcion
                    FROM usuario, auxiliar
                    WHERE usuario.id = auxiliar.usuarioId && auxiliar.proyectoId = ${idProyecto}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const auxiliarRaw = ds.connector.fromRow('auxiliar', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const auxiliar = new Usuario.app.models.auxiliar(auxiliarRaw);

                // Set the auxiliar on the book object
                usuario.auxiliar(auxiliar);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('publicAuxiliaresPorProyecto', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.auxiliar = result.__cachedRelations.auxiliar.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicAuxiliaresPorProyecto', {
        accepts: [
            { arg: 'idProyecto', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:idProyecto/publicAuxiliaresPorProyecto', verb: 'get', status: 200 }
    });

    /* 23.- ------------- AQ Retorna TODOS los usuarios AuxiliaresInvestigadores de una CONVOCATORIA incluido auxiliar -------------*/
    Usuario.auxiliaresPorConvocatoria = function(idConvocatoria, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.*, auxiliar.*
                    FROM usuario, auxiliar
                    WHERE usuario.id = auxiliar.usuarioId && auxiliar.convocatoriainvesId = ${idConvocatoria}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const auxiliarRaw = ds.connector.fromRow('auxiliar', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const auxiliar = new Usuario.app.models.auxiliar(auxiliarRaw);

                // Set the auxiliar on the book object
                usuario.auxiliar(auxiliar);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('auxiliaresPorConvocatoria', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.auxiliar = result.__cachedRelations.auxiliar.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('auxiliaresPorConvocatoria', {
        accepts: [
            { arg: 'idConvocatoria', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:idConvocatoria/auxiliaresPorConvocatoria', verb: 'get', status: 200 }
    });

    /* 22.-   ------------- Retorna datos si es DOCENTE INVESTIGADOR por su username // revisar antes de liberar -------------*/
    Usuario.rolem = function(username, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT rolemapping.id 
                    FROM usuario, rolemapping
                    WHERE rolemapping.principalId = usuario.id && usuario.username = '${username}'`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            cb(null, resultObjects);
        });
    };
    Usuario.remoteMethod('rolem', {
        accepts: [
            { arg: 'username', type: 'string', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/rolem/:username', verb: 'get', status: 200 }
    });

    /* 21.-   ------------- Retorna datos si es DOCENTE INVESTIGADOR por su username -------------*/
    Usuario.investigator = function(username, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT investiga.id 
                    FROM usuario, investigador, investiga
                    WHERE investiga.investigadorId = investigador.id && investigador.usuarioId = usuario.id && usuario.username = '${username}'`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            cb(null, resultObjects);
        });
    };
    Usuario.remoteMethod('investigator', {
        accepts: [
            { arg: 'username', type: 'string', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/investigator/:username', verb: 'get', status: 200 }
    });

    /* 20.-   ------------- Retorna datos publicos de UN INVESTIGADOR por su codimagen -------------*/
    Usuario.publicInvestigador = function(gestion, docinv, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    investigador.grado, investigador.resena, investigador.imgpath, investigador.descripcion, investigador.denominacion
                    FROM convocatoriainves, investiga, usuario, investigador 
                    WHERE convocatoriainves.gestion = '${gestion}' && convocatoriainves.id = investiga.convocatoriainvesId
                    && investiga.investigadorId = investigador.id && investigador.denominacion = '${docinv}' && investigador.usuarioId = usuario.id`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const investigadorRaw = ds.connector.fromRow('investigador', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const investigador = new Usuario.app.models.investigador(investigadorRaw);

                // Set the investigador on the book object
                usuario.investigador(investigador);
                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('publicInvestigador', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.investigador = result.__cachedRelations.investigador.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicInvestigador', {
        accepts: [
            { arg: 'gestion', type: 'string', required: true },
            { arg: 'docinv', type: 'string', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:gestion/publicInvestigador/:docinv', verb: 'get', status: 200 }
    });

    /* 19.- ------------- AQ Retorna TODOS los usuarios DocentesInvestigadores de una CONVOCATORIA incluido investigador -------------*/
    Usuario.publicInvestigadoresPorConvocatoria = function(gestionConv, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    investigador.grado, investigador.resena, investigador.imgpath, investigador.descripcion, investigador.denominacion
                    FROM usuario, investigador, investiga, convocatoriainves
                    WHERE usuario.id = investigador.usuarioId && investigador.id = investiga.investigadorId 
                    && investiga.convocatoriainvesId = convocatoriainves.id && convocatoriainves.gestion = ${gestionConv}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const investigadorRaw = ds.connector.fromRow('investigador', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const investigador = new Usuario.app.models.investigador(investigadorRaw);

                // Set the investigador on the book object
                usuario.investigador(investigador);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('publicInvestigadoresPorConvocatoria', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.investigador = result.__cachedRelations.investigador.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicInvestigadoresPorConvocatoria', {
        accepts: [
            { arg: 'gestionConv', type: 'string', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:gestionConv/publicInvestigadoresPorConvocatoria', verb: 'get', status: 200 }
    });

    /* 18.- ------------- Retorna el nombre DocentesInvestigadores de investigaron un Proyecto incluido investigador -------------*/
    Usuario.publicInvestigadoresPorProyecto = function(idProyecto, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    investigador.grado, investigador.resena, investigador.imgpath, investigador.descripcion, investigador.denominacion
                    FROM usuario, investigador, investiga, publica
                    WHERE usuario.id = investigador.usuarioId && investigador.id = investiga.investigadorId
                     && investiga.id = publica.investigaId && publica.proyectoId = ${idProyecto}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const investigadorRaw = ds.connector.fromRow('investigador', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const investigador = new Usuario.app.models.investigador(investigadorRaw);

                // Set the investigador on the book object
                usuario.investigador(investigador);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('publicInvestigadoresPorProyecto', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.investigador = result.__cachedRelations.investigador.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicInvestigadoresPorProyecto', {
        accepts: [
            { arg: 'idProyecto', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:idProyecto/publicInvestigadoresPorProyecto', verb: 'get', status: 200 }
    });

    // Revisar no es necesario intruducir el idConvocatoria ya que el propytecto solo puede pertenecer a una convocatoria
    /* 17.- ------------- AQ Retorna TODOS los usuarios DocentesInvestigadores de investigaron un Proyecto incluido docente -------------*/
    Usuario.docsInvestigadoresPorProyecto = function(idConvocatoria, idProyecto, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.*, docente.*
                    FROM usuario, docente, investiga, publica
                    WHERE usuario.id = docente.usuarioId && docente.id = investiga.docenteId
                        && investiga.convocatoriainvesId = ${idConvocatoria} && investiga.id = publica.investigaId
                        && publica.proyectoId = ${idProyecto}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const docenteRaw = ds.connector.fromRow('docente', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const docente = new Usuario.app.models.docente(docenteRaw);

                // Set the docente on the book object
                usuario.docente(docente);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('docsInvestigadoresPorProyecto', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.docente = result.__cachedRelations.docente.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('docsInvestigadoresPorProyecto', {
        accepts: [
            { arg: 'idConvocatoria', type: 'number', required: true },
            { arg: 'idProyecto', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:idConvocatoria/:idProyecto/docsInvestigadoresPorProyecto', verb: 'get', status: 200 }
    });

    /* 16.- ------------- AQ Retorna TODOS los usuarios DocentesInvestigadores de una CONVOCATORIA incluido investigador -------------*/
    Usuario.investigadoresPorConvocatoria = function(idConvocatoria, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.*, investigador.*
                    FROM usuario, investigador, investiga, convocatoriainves
                    WHERE usuario.id = investigador.usuarioId && investigador.id = investiga.investigadorId 
                    && investiga.convocatoriainvesId = convocatoriainves.id && convocatoriainves.id = ${idConvocatoria}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const investigadorRaw = ds.connector.fromRow('investigador', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const investigador = new Usuario.app.models.investigador(investigadorRaw);

                // Set the investigador on the book object
                usuario.investigador(investigador);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('investigadoresPorConvocatoria', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.investigador = result.__cachedRelations.investigador.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('investigadoresPorConvocatoria', {
        accepts: [
            { arg: 'idConvocatoria', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/:idConvocatoria/investigadoresPorConvocatoria', verb: 'get', status: 200 }
    });

    /* 15.- ------------- AQ Retorna TODOS los usuarios DocentesInvestigadores incluido docente -------------*/
    Usuario.investigadores = function(aq, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT DISTINCT usuario.*, investigador.*
                    FROM usuario, investigador, investiga WHERE usuario.id = investigador.usuarioId && investigador.id = investiga.investigadorId`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb(null, []);

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const investigadorRaw = ds.connector.fromRow('investigador', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const investigador = new Usuario.app.models.investigador(investigadorRaw);

                // Set the investigador on the book object
                usuario.investigador(investigador);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('investigadores', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.investigador = result.__cachedRelations.investigador.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('investigadores', {
        accepts: [
            { arg: 'aqc', type: 'string' },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/investigadores', verb: 'get', status: 200 }
    });


    /*14. ------------- Eliminar el registro de la Tabla RoleMapping que pertenecia al docente o estudiante eliminado  -------------*/
    Usuario.observe('after delete', function(ctx, next) {

        console.log('Eliminar %s corresponde %j',
            ctx.Model.pluralModelName,
            ctx.where.id);

        var RoleMapping = app.models.RoleMapping;
        RoleMapping.find({ where: { principalId: ctx.where.id } }, function(err, resultObjects) {

            var lago = resultObjects.map(resultRaw => {
                console.log(resultRaw.id);
                RoleMapping.destroyById(resultRaw.id, function(err) {
                    var response = "Successfully removed";
                    console.log(response);
                });
            });

        });

        next();
    });

    /* 13. ------------- Método que retorna las notas de Un Estudiante que tomo Un Curso -------------*/
    Usuario.notasCurso = function(id, idCurso, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT nota.*
                    FROM usuario, estudiante, nota, modulo, curso
                    WHERE ${id} = usuario.id && usuario.id = estudiante.usuarioId && estudiante.id = nota.estudianteId
                                && nota.moduloId = modulo.id && modulo.cursoId = curso.id && curso.id = ${idCurso}`;


        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Usuario.remoteMethod('notasCurso', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
            { arg: 'idCurso', type: 'number', required: true }
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/:idCurso/notasCurso', verb: 'get', status: 200 }
    });

    /* 12. ------------- Método que retorna los cursos que esta tomando un Estudiante de posgrado -------------*/
    Usuario.cursosInscrito = function(id, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT DISTINCT curso.*
                    FROM curso, modulo, estudiante, usuario, nota
                    WHERE ${id} = usuario.id && usuario.id = estudiante.usuarioId && estudiante.id = nota.estudianteId
                            && nota.moduloId = modulo.id && modulo.cursoId = curso.id`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Usuario.remoteMethod('cursosInscrito', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/cursosInscrito', verb: 'get', status: 200 }
    });

    /* 11. ------------- Propiedad que retorna los modulos de un curso en los que un docente dicta catedra -------------*/
    Usuario.dictaModulosCurso = function(id, idCurso, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT modulo.*
                    FROM modulo, curso, docente, usuario
                    WHERE curso.id = ${idCurso} && curso.id = modulo.cursoId && modulo.docenteId = docente.id
                        && docente.usuarioId =  usuario.id && usuario.id = ${id}`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Usuario.remoteMethod('dictaModulosCurso', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
            { arg: 'idCurso', type: 'number', required: true }
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/:idCurso/dictaModulosCurso', verb: 'get', status: 200 }
    });

    /* 10. ------------- Propiedad que retorna los cursos en lo que un docente dicta catedra *no importa la cantidad de módulos -------------*/
    Usuario.dictaCursos = function(id, cb) {

        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT DISTINCT curso.*
                    FROM curso, modulo, docente, usuario
                    WHERE curso.id = modulo.cursoId && modulo.docenteId = docente.id
                        && docente.usuarioId = usuario.id && usuario.id = ${id}`;

        ds.connector.execute(sql, (err, resultObjects) => {

            if (err) {
                console.log(err);
            } else {
                cb(null, resultObjects);
            }
        });
    }
    Usuario.remoteMethod('dictaCursos', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/dictaCursos', verb: 'get', status: 200 }
    });

    /* 9.- ------------- AQ Metodo que asigna el ROLE 'admin' a un determinado usuario mediante un ENDPOINT -------------*/
    Usuario.remoteMethod('aqmaadm', {
        accepts: [
            { arg: 'id', type: 'number', required: true },
        ],
        returns: { type: 'array', root: true },
        http: { path: '/:id/aqmaadm', verb: 'put', status: 200 }
    });

    Usuario.aqmaadm = function(id, cb) {

        var Role = app.models.Role;
        var RoleMapping = app.models.RoleMapping;
        Role.findOne({ where: { name: 'admin' } }, function(err, result) {
            if (err) {
                return console.log(err);
            } else {

                RoleMapping.findOrCreate({ where: { principalId: id, roleId: result.id } }, {
                    principalType: RoleMapping.USER,
                    principalId: id,
                    roleId: result.id
                }, function(err, principal) {
                    // TODO handle error
                    if (err) throw err;
                    console.log('Created principal:', principal);
                    cb(null, principal);
                    // if no errors, user has now the role administrator
                });
            }
        });
    };

    /* 8.- ------------- AQ Retorna TODOS los datos de TODOS los usuarios incluido estudiante x Sigla-------------*/
    Usuario.privadoEstudiantesPorSigla = function(sigla, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT DISTINCT usuario.*, estudiante.*
                    FROM usuario, estudiante, nota, modulo, curso
                    WHERE usuario.id = estudiante.usuarioId && estudiante.id = nota.estudianteId &&
                        nota.moduloId = modulo.id && modulo.cursoId = curso.id && curso.sigla = ${sigla}
                    ORDER BY usuario.paterno`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const estudianteRaw = ds.connector.fromRow('estudiante', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const estudiante = new Usuario.app.models.estudiante(estudianteRaw);

                // Set the estudiante on the book object
                usuario.estudiante(estudiante);
                return usuario;
            });

            cb(null, usuarios);
        });
    }

    Usuario.afterRemote('privadoEstudiantesPorSigla', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.estudiante = result.__cachedRelations.estudiante.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('privadoEstudiantesPorSigla', {
        accepts: [
            { arg: 'sigla', type: 'string', required: true },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/privadoEstudiantesPorSigla/:sigla', verb: 'get', status: 200 }
    });

    /* 7.- ------------- AQ Retorna estudiantesId no repetidos por Sigla y Numero de Módulo, que no pertenecen al modulo-------------*/
    Usuario.estudiantesPorSiglaIdModuloComplemento = function(cursoSigla, IdModulo, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT t.moduloId, t.estudianteId
                    FROM
                        (SELECT nota.*
                        from nota, modulo, curso
                        where nota.moduloId <> ${IdModulo} && nota.moduloId = modulo.id &&
                        modulo.cursoId = curso.id && curso.sigla = ${cursoSigla}
                        GROUP BY nota.estudianteId) AS t
                    LEFT JOIN
                        (SELECT nota.*
                        from nota
                        where nota.moduloId = ${IdModulo}) AS f
                    ON t.estudianteId = f.estudianteId
                    WHERE f.estudianteId IS null`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            cb(null, resultObjects);
        });
    };

    Usuario.remoteMethod('estudiantesPorSiglaIdModuloComplemento', {
        accepts: [
            { arg: 'cursoSigla', type: 'string', required: true },
            { arg: 'IdModulo', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/estudiantesPorSiglaIdModuloComplemento/:cursoSigla/:IdModulo', verb: 'get', status: 200 }
    });



    /* 6.- ------------- AQ Metodo que retorna TODOS los datos de TODOS los usuario incluido estudiante ingresando SIGLA Y NUMEROMODULO-------------*/
    Usuario.estudiantesPrivadoSiglaNumero = function(cursoSigla, numeroModulo, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.*, estudiante.*
                    FROM usuario, estudiante, nota, modulo, curso
                    WHERE usuario.id = estudiante.usuarioId && estudiante.id = nota.estudianteId &&
                        nota.moduloId = modulo.id && modulo.cursoId = curso.id && modulo.numero = ${numeroModulo} &&
                        curso.sigla = ${cursoSigla}
                    ORDER BY usuario.paterno`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const estudianteRaw = ds.connector.fromRow('estudiante', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const estudiante = new Usuario.app.models.estudiante(estudianteRaw);

                // Set the estudiante on the book object
                usuario.estudiante(estudiante);

                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('estudiantesPrivadoSiglaNumero', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.estudiante = result.__cachedRelations.estudiante.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('estudiantesPrivadoSiglaNumero', {
        accepts: [
            { arg: 'cursoSigla', type: 'string', required: true },
            { arg: 'numeroModulo', type: 'number', required: true }
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/estudiantesPrivadoSiglaNumero/:cursoSigla/:numeroModulo', verb: 'get', status: 200 }
    });


    /* 5.- ------------- AQ Metodo que retorna TODOS los datos de TODOS los usuario incluido docente POR CURSO-------------*/
    Usuario.privadoDocentesPorCurso = function(cursoId, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.*, docente.*
                    FROM cursomodulo, modulo, usuario, docente
                    WHERE cursomodulo.cursoId = ${cursoId} && cursomodulo.moduloId = modulo.id &&
                            modulo.docenteId = docente.id && docente.aqusuarioid = usuario.id`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const docenteRaw = ds.connector.fromRow('docente', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const docente = new Usuario.app.models.docente(docenteRaw);

                // Set the docente on the book object
                usuario.docente(docente);
                return usuario;
            })

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('privadoDocentesPorCurso', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObject = result.toJSON();
            resultObject.docente = result.__cachedRelations.docente.toJSON();
            return resultObject;
        });
        cb();
    });
    Usuario.remoteMethod('privadoDocentesPorCurso', {
        accepts: [
            { arg: 'cursoId', type: 'number', required: 'true' },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/privadoDocentesPorCurso/:cursoId', verb: 'get', status: 200 }
    });

    /* 4.- ------------- AQ Metodo que retorna los datos privados de UN usuario incluido docente -------------*/
    Usuario.privadoDocente = function(docenteId, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT docente.*, usuario.*
                    FROM docente, usuario 
                        WHERE usuario.id = docente.usuarioId && docente.id =  ${docenteId}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const docenteRaw = ds.connector.fromRow('docente', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const docente = new Usuario.app.models.docente(docenteRaw);

                // Set the docente on the book object
                usuario.docente(docente);
                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('privadoDocente', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.docente = result.__cachedRelations.docente.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('privadoDocente', {
        accepts: [
            { arg: 'docenteId', type: 'number', required: true },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/privadoDocente/:docenteId', verb: 'get', status: 200 }
    });

    // DEL ULTIMO SELECT DEPENDE EL NUMEROID QUE SALGA EN ESTE CASO EL ID DEL DOCENTE
    /* 3.- ------------- AQ Metodo que retorna TODOS los datos de TODOS los usuario incluido docente -------------*/
    Usuario.privadoDocentes = function(aqc, cb) {
        var ds = app.dataSources.mysqlDB;
        // var sql = `select usuario.*, docente.*
        //              from usuario, docente
        //                 where docente.usuarioId = usuario.id`;
        var sql = `SELECT DISTINCT usuario.*, docente.*
                    from usuario, docente, modulo
                    where modulo.docenteId = docente.id && docente.usuarioId = usuario.id`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const docenteRaw = ds.connector.fromRow('docente', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const docente = new Usuario.app.models.docente(docenteRaw);

                // Set the docente on the book object
                usuario.docente(docente);
                return usuario;
            })

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('privadoDocentes', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObject = result.toJSON();
            resultObject.docente = result.__cachedRelations.docente.toJSON();
            return resultObject;
        });
        cb();
    });
    Usuario.remoteMethod('privadoDocentes', {
        accepts: [
            { arg: 'aqc', type: 'any' },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/privadoDocentes', verb: 'get', status: 200 }
    });


    /* 2.- ------------- AQ Metodo que retorna los datos publicos de TODOS los usuarios incluido admin -------------*/
    Usuario.publicAdmins = function(aqc, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    admin.cargo, admin.grado, admin.imgpath, admin.descripcion, admin.vista_posicion
                    FROM usuario, admin 
                        WHERE usuario.id = admin.usuarioId
                            ORDER BY admin.vista_posicion`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            // console.log(resultObjects);
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const adminRaw = ds.connector.fromRow('admin', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const admin = new Usuario.app.models.admin(adminRaw);

                // Set the admin on the book object
                usuario.admin(admin);
                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('publicAdmins', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.admin = result.__cachedRelations.admin.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicAdmins', {
        accepts: [
            { arg: 'aqc', type: 'number' },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/publicAdmins', verb: 'get', status: 200 }
    });

    /* 1.-   ------------- AQ Metodo que retorna los datos publicos de UN usuario incluido admin -------------*/
    Usuario.publicAdmin = function(adminCargo, cb) {
        var ds = app.dataSources.mysqlDB;
        var sql = `SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email,
                    admin.cargo, admin.resena, admin.grado, admin.imgpath, admin.descripcion, admin.oficina
                    FROM usuario, admin 
                        WHERE usuario.id = admin.usuarioId && admin.cargo = ${adminCargo}`;

        ds.connector.execute(sql, null, (err, resultObjects) => {
            if (err) return cb(err);
            if (!resultObjects.length) return cb();

            const usuarios = resultObjects.map(resultRaw => {
                const usuarioRaw = ds.connector.fromRow('usuario', resultRaw);
                const adminRaw = ds.connector.fromRow('admin', resultRaw);
                const usuario = new Usuario.app.models.usuario(usuarioRaw);
                const admin = new Usuario.app.models.admin(adminRaw);

                // Set the admin on the book object
                usuario.admin(admin);
                return usuario;
            });

            cb(null, usuarios);
        });
    };

    Usuario.afterRemote('publicAdmin', function(ctx, _results, cb) {
        ctx.result = ctx.result.map(result => {
            const resultObj = result.toJSON();
            resultObj.admin = result.__cachedRelations.admin.toJSON();
            return resultObj;
        });
        cb();
    });
    Usuario.remoteMethod('publicAdmin', {
        accepts: [
            { arg: 'adminCargo', type: 'string', required: true },
        ],
        returns: { type: 'Usuario', root: true },
        http: { path: '/publicAdmin/:adminCargo', verb: 'get', status: 200 }
    });

}





// Post.find({
//     include: {
//         relation: 'admin', // include the owner object
//         scope: { // further filter the owner object
//             fields: ['paterno', 'email'], // only show two fields
//             // include: { // include orders for the owner
//             //   relation: 'orders', 
//             //   scope: {
//             //     where: {orderId: 5} // only select order with id 5
//             //   }
//             // }
//         }
//     }
// }, function() { /* ... */ });

// Usuario.find({
//     include: {
//         relation: 'admin', // include the owner object
//         scope: { // further filter the owner object
//             fields: ['paterno', 'email'], // only show two fields
//             // include: { // include orders for the owner
//             //   relation: 'orders', 
//             //   scope: {
//             //     where: {orderId: 5} // only select order with id 5
//             //   }
//             // }
//         }
//     }
// }, function(err, callback) { console.log(callback); });

// Usuario.remoteMethod(
//     'aqusadpudas', {
//         accepts: { arg: 'filter', type: 'number' },
//         http: { path: '/aqusadpudas', verb: 'get' },
//         returns: { arg: 'aqusadpudas', type: 'Object' }
//     });
// Usuario.aqusadpudas = function(filter, cb) {
//     var ds = app.dataSources.mysqlDB;
//     // var query = "SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email, admin.id AS adminId, admin.cargo, admin.estudios, admin.desempeno, admin.imgpath, admin.usuarioId FROM usuario, admin where usuario.id = admin.usuarioId";
//     var query = "SELECT * FROM usuario";


//     ds.connector.execute(query, function(err, data) {
//         if (err) {
//             console.log(err);
//         } else {
//             var filter = { include: { relation: 'admin' } };
//             var filtered = require('loopback-filters')(data, filter);
//             console.log(filtered);
//             cb(null, filtered);
//             // data.filter(function(item) {
//             //     return item.price < userInput.min && item.price >= userInput.max
//             //   });
//         }
//     });
// };


// Usuario.remoteMethod('getAllGlobalFilters', {
//     'accepts': [{ 'arg': 'filter', 'type': 'string' }],
//     'http': {
//         'path': '/getAllGlobalFilters',
//         'verb': 'get'
//     },
//     'returns': { 'arg': 'allGlobalFilters', 'type': 'array' }
// });

// Usuario.getAllGlobalFilters = function(filter, callback) {
//     filter = JSON.parse(filter);
//     this.find(filter, function(err, data) {
//         if (err) {
//             return callback(err);
//         }
//         return callback(null, data);
//     });
// };

// Usuario.remoteMethod('getAllGlobalFilters', {
//     'accepts': [{ 'arg': 'filter', 'type': 'string' }],
//     'http': {
//         'path': '/getAllGlobalFilters',
//         'verb': 'get'
//     },
//     'returns': { 'arg': 'allGlobalFilters', 'type': 'array' }
// });
// Usuario.getAllGlobalFilters = function(filter, callback) {
//     var ds = app.dataSources.mysqlDB;
//     var query = "SELECT usuario.paterno FROM usuario, admin where usuario.id = admin.usuarioId";
//     ds.connector.execute(query, function(err, data) {
//         if (err) {
//             console.log(err);
//         } else {
//             // callback(null, data);
//             filter = JSON.parse(filter);
//             this.find(filter, function(err, data) {
//                 if (err) {
//                     return callback(err);
//                 }
//                 return callback(null, data);
//             });
//         }
//     });

// };

// }