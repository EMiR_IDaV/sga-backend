'use strict';
var app = require('../../server/server');

module.exports = function(Admin) {

    /*------------- AQ Asignar role admin al admin creado o actualizado  MEJORADO-------------*/
    // Admin.observe('after save', function(ctx, next) {
    //     var Role = app.models.Role;
    //     var RoleMapping = app.models.RoleMapping;
    //     Role.findOne({ where: { name: 'admin' } }, function(err, result) {
    //         if (err) {
    //             return console.log(err);
    //         } else {

    //             RoleMapping.findOrCreate({ where: { principalId: ctx.instance.usuarioId, roleId: result.id } }, {
    //                 principalType: RoleMapping.USER,
    //                 principalId: ctx.instance.usuarioId,
    //                 roleId: result.id
    //             }, function(err, principal) {
    //                 // TODO handle error
    //                 if (err) throw err;
    //                 console.log('Created principal:', principal);
    //                 // if no errors, user has now the role administrator
    //             });
    //         }
    //     });
    //     next();
    // });


    // AQ -> Retorna datos públicos del Usuario
    Admin.remoteMethod(
        'aqusadpudas', {
            accepts: { arg: 'aq', type: 'number' },
            http: { path: '/aqusadpudas', verb: 'get' },
            returns: { arg: 'aqusadpudas', type: 'Object' }
        });
    Admin.aqusadpudas = function(aq, cb) {

        var ds = app.dataSources.mysqlDB;
        var query = "SELECT usuario.paterno, usuario.materno, usuario.pri_nombre, usuario.seg_nombre, usuario.email, admin.id AS adminId, admin.cargo, admin.descripcion, admin.imgpath, admin.usuarioId FROM usuario, admin where usuario.id = admin.usuarioId";

        ds.connector.execute(query, function(err, data) {

            if (err) {
                console.log(err);
            } else {
                cb(null, data);
            }
        });
    }

    Admin.greet = function(msg, cb) {
        cb(null, 'Greetings... ' + msg);
    }

    Admin.remoteMethod('greet', {
        isStatic: true,
        accepts: { arg: 'msg', type: 'string' },
        returns: { arg: 'greeting', type: 'string' }
    });
};